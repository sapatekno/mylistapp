package id.co.iconpln.mylistapp

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_hero.*

class DetailHeroActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_HERO = "extra_hero"
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMAGE_URL = "extra_image_url"
    }

    private lateinit var hero: Hero


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hero)

        setupActionBar()
        displayHeroDetail()
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Detail Hero"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displayHeroDetail() {
        hero = intent.getParcelableExtra(EXTRA_HERO)

        tvHeroDetailName.text = hero.name
        tvHeroDetailDesc.text = hero.desc
        Glide.with(this)
            .load(hero.photo)
            .apply(
                RequestOptions()
                    .centerInside()
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_foreground)
            )
            .into(ivHeroDetailImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                false
            }
        }
    }
}
