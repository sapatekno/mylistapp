package id.co.iconpln.mylistapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val listHero: ListView get() = lv_list_hero
    private val list: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListItemClickListener(listHero)
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                Toast.makeText(this, this.list[position].name, Toast.LENGTH_SHORT).show()
                showDetailHero(list[position])
            }
    }

    private fun showDetailHero(hero: Hero) {
        val detailHeroActivity = Intent(this, DetailHeroActivity::class.java)
        detailHeroActivity.putExtra(DetailHeroActivity.EXTRA_HERO, hero)
        startActivity(detailHeroActivity)
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(HeroesData.listDataHero)

        val baseAdapter = ListViewHeroAdapter(context, list)
        listHero.adapter = baseAdapter
    }

    private fun loadListArrayAdapter() {
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataHero())
        listHero.adapter = adapter
    }

    private fun getDataHero(): Array<String> {
        return arrayOf(
            "Ki Hajar Dewantara",
            "Kapiten Patimura",
            "Pangeran Diponegoro",
            "Soekarno",
            "Cut Nyak Dien",
            "R.A. Kartini"
        )
    }
}
